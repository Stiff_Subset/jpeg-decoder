#include "matrix.h"
#include "qt_ht_decoder.h"

#include <glog/logging.h>

#include <vector>

const int kLayout[64] = {0,  1,  8,  16, 9,  2,  3,  10, 17, 24, 32, 25, 18, 11, 4,  5,
                         12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6,  7,  14, 21, 28,
                         35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51,
                         58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63};

void ParseSingleQuantizationTable(BitReader& reader, int& length, State& state) {
    int bit_size, id;
    int value = reader.ReadByte();
    Split(value, bit_size, id);
    DLOG(INFO) << "Id of current quantization table is " << id;
    DLOG(INFO) << "Each element of table decode using " << bit_size + 1 << " bytes";
    if (bit_size > 1) {
        throw std::invalid_argument{"Invalid bit size of quantization table"};
    }
    if (id > 1) {
        throw std::invalid_argument{"Invalid id number of quantization table"};
    }
    int read_bytes = 64 * (1 + bit_size) + 1;
    if (length < read_bytes) {
        throw std::invalid_argument{"Invalid DQT format"};
    }
    for (int i = 0; i < 64; ++i) {
        int value = (bit_size == 0 ? reader.ReadByte() : reader.ReadWord());
        state.quant_table[id].At(kLayout[i]) = value;
    }
    DLOG(INFO) << id << "th quantization table:";
    // state.quant_table[id].Print();
    state.quant_table[id].Assign();
    length -= read_bytes;
}

void ParseQuantizationTable(BitReader& reader, State& state) {
    int length = reader.ReadWord() - 2;
    DLOG(INFO) << "Length of current DQT section is " << length;
    while (length != 0) {
        ParseSingleQuantizationTable(reader, length, state);
    }
    reader.ReleaseLast();
}

void ParseSingleHuffmanTree(BitReader& reader, int& length, State& state) {
    int type, id;
    int value = reader.ReadByte();
    Split(value, type, id);
    DLOG(INFO) << "Current huffman tree used for " << (type == 0 ? "DC" : "AC") << " coefficients";
    DLOG(INFO) << "Current huffman tree's id is " << id;
    if (type > 1) {
        throw std::invalid_argument{"Invalid type of huffman tree"};
    }
    if (id > 1) {
        throw std::invalid_argument{"Invalid id of huffman tree"};
    }
    std::vector<uint8_t> code_lengths(16);
    int count_values = 0;
    for (int i = 0; i < 16; ++i) {
        code_lengths[i] = reader.ReadByte();
        count_values += code_lengths[i];
    }
    int read_bytes = 1 + 16 + count_values;
    if (length < read_bytes) {
        throw std::invalid_argument{"Invalid DHT format"};
    }
    std::vector<uint8_t> values(count_values);
    for (int i = 0; i < count_values; ++i) {
        values[i] = reader.ReadByte();
    }
    DLOG(INFO) << "Current code_lengths vector:";
    // for (int x : code_lengths) {
    //     std::cerr << x << " ";
    // }
    // std::cerr << std::endl;
    // DLOG(INFO) << "Current values:";
    // for (int x : values) {
    //     std::cerr << x << " ";
    // }
    // std::cerr << std::endl;
    state.huffman_tree[type][id].Build(code_lengths, values);
    DLOG(INFO) << "Huffman tree was build";
    length -= read_bytes;
}

void ParseHuffmanTree(BitReader& reader, State& state) {
    int length = reader.ReadWord() - 2;
    DLOG(INFO) << "Length of current DHT section is " << length;
    while (length != 0) {
        ParseSingleHuffmanTree(reader, length, state);
    }
    reader.ReleaseLast();
}
