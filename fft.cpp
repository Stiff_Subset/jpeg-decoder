#include "fft.h"

#include <fftw3.h>

#include <cmath>
#include <stdexcept>

DctCalculator::DctCalculator(size_t width, std::vector<double> *input,
                             std::vector<double> *output) {
    if ((*input).size() != width * width) {
        throw std::invalid_argument{"Wrong input size"};
    }
    if ((*output).size() != width * width) {
        throw std::invalid_argument{"Wrong output size"};
    }
    width_ = width;
    input_ = input;
    output_ = output;
}

void DctCalculator::Inverse() {
    for (size_t i = 0; i < width_; ++i) {
        for (size_t j = 0; j < width_; ++j) {
            size_t ind = width_ * i + j;
            if (i == 0) {
                (*input_)[ind] *= sqrt(2.0);
            }
            if (j == 0) {
                (*input_)[ind] *= sqrt(2.0);
            }
            (*input_)[ind] /= 16.0;
        }
    }
    fftw_plan p;
    p = fftw_plan_r2r_2d(width_, width_, (*input_).data(), (*output_).data(), FFTW_REDFT01,
                         FFTW_REDFT01, FFTW_ESTIMATE | FFTW_DESTROY_INPUT);
    fftw_execute(p);
    fftw_destroy_plan(p);
}
