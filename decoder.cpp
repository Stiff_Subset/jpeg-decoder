#include "decoder.h"
#include "mcu.h"
#include "qt_ht_decoder.h"

#include <cmath>
#include <iostream>
#include <stdexcept>

#include <glog/logging.h>

const unsigned char kZO = 0x00;
const unsigned char kFF = 0xFF;
const unsigned char kSOI = 0xD8;
const unsigned char kCOM = 0xFE;
const unsigned char kAPPl = 0xE0;
const unsigned char kAPPr = 0xEF;
const unsigned char kDQT = 0xDB;
const unsigned char kSOF0 = 0xC0;
const unsigned char kDHT = 0xC4;
const unsigned char kSOS = 0xDA;
const unsigned char kEOI = 0xD9;
const int kLayout[64] = {0,  1,  8,  16, 9,  2,  3,  10, 17, 24, 32, 25, 18, 11, 4,  5,
                         12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6,  7,  14, 21, 28,
                         35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51,
                         58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63};

////////////////////////////////////////////////////////////////////////////////////////////

void ParseApplicationSpecificData(BitReader& reader) {
    int length = reader.ReadWord() - 2;
    reader.Shift(length);
    DLOG(INFO) << "Skipped " << length << " bytes in APPn section";
}

void ParseComment(BitReader& reader, Image& image) {
    int length = reader.ReadWord() - 2;
    std::string comment(length, '-');
    for (int i = 0; i < length; ++i) {
        comment[i] = reader.ReadByte();
    }
    image.SetComment(comment);
    DLOG(INFO) << "Found comment " << comment;
}

void ParseImageInfo(BitReader& reader, Image& image, State& state) {
    int length = reader.ReadWord() - 2;
    DLOG(INFO) << "Length of image info section is " << length;
    int precision = reader.ReadByte();
    DLOG(INFO) << "Image precision is " << precision;
    if (precision != 8) {
        throw std::invalid_argument{"Unsupported precision"};
    }
    int height = reader.ReadWord();
    int width = reader.ReadWord();
    if (height == 0 || width == 0) {
        throw std::invalid_argument{"Invalid dimensions of picture"};
    }
    DLOG(INFO) << "Image size is " << height << " x " << width;
    image.SetSize(width, height);
    int component_number = reader.ReadByte();
    DLOG(INFO) << "Number of components is " << component_number;
    if (component_number > 3) {
        throw std::invalid_argument{"Unsupported number of components"};
    }
    state.max_hor_fac = 0;
    state.max_ver_fac = 0;
    state.components = component_number;
    for (int i = 0; i < component_number; ++i) {
        int id = reader.ReadByte();
        DLOG(INFO) << "Id of current component is " << id;
        if (id < 1 || id > 3) {
            throw std::invalid_argument{"Invalid id of component"};
        }
        int hor_fac, ver_fac;
        int value = reader.ReadByte();
        Split(value, hor_fac, ver_fac);
        DLOG(INFO) << "Horizontal factor is " << hor_fac;
        DLOG(INFO) << "Vertical factor is " << ver_fac;
        int quant_table_id = reader.ReadByte();
        DLOG(INFO) << "Quantization table index is " << quant_table_id;
        if (quant_table_id > 1) {
            throw std::invalid_argument{"Invalid quantization table id for current component"};
        }
        state.max_hor_fac = std::max(state.max_hor_fac, hor_fac);
        state.max_ver_fac = std::max(state.max_ver_fac, ver_fac);
        state.dqt_id[id - 1] = quant_table_id;
        DLOG(INFO) << "-------------------";
    }
    DLOG(INFO) << "Max horizontal factor is " << state.max_hor_fac;
    DLOG(INFO) << "Max vertical factor is " << state.max_ver_fac;
}

RGB YCbCrtoRGB(int y, int cb, int cr) {
    int r = std::round((y + .0) + 1.402 * (cr - 128));
    int g = std::round((y + .0) - 0.344136 * (cb - 128) - 0.714136 * (cr - 128));
    int b = std::round((y + .0) + 1.772 * (cb - 128));
    r = std::min(std::max(0, r), 255);
    g = std::min(std::max(0, g), 255);
    b = std::min(std::max(0, b), 255);
    return {r, g, b};
}

void ParseMainSection(BitReader& reader, Image& image, State& state) {
    reader.SetSkip();
    int length = reader.ReadWord() - 2;
    DLOG(INFO) << "Length of info about SOS section is " << length;
    int components_number = reader.ReadByte();
    DLOG(INFO) << "Number of components is " << components_number;
    if (components_number > 3) {
        throw std::invalid_argument{"Unsupported number of components"};
    }
    if (components_number != state.components) {
        throw std::invalid_argument{"Number of components are different in SOF0 and SOS"};
    }
    DLOG(INFO) << length - 4;
    for (int i = 0; i < length - 4; i += 2) {
        int component_id = reader.ReadByte();
        DLOG(INFO) << "Current component is " << component_id;
        if (component_id > 3) {
            throw std::invalid_argument{"Invalid component id"};
        }
        int dc_id, ac_id;
        int value = reader.ReadByte();
        Split(value, dc_id, ac_id);
        DLOG(INFO) << "Id of DC huffman tree is " << dc_id;
        DLOG(INFO) << "Id of AC huffman tree is " << ac_id;
        state.acdc_id[2 * (component_id - 1)] = dc_id;
        state.acdc_id[2 * (component_id - 1) + 1] = ac_id;
        DLOG(INFO) << "-------------------";
        if (dc_id > 1) {
            throw std::invalid_argument{"Id of huffman tree for DC coefficient is invalid"};
        }
        if (ac_id > 1) {
            throw std::invalid_argument{"Id of huffman tree for AC coefficient is invalid"};
        }
    }
    int progressive1 = reader.ReadByte();
    int progressive2 = reader.ReadByte();
    int progressive3 = reader.ReadByte();
    if (progressive1 != 0 || progressive2 != 0x3F || progressive3 != 0) {
        throw std::invalid_argument{"Invalid markers for progressive"};
    }
    DLOG(INFO) << "Start reading raw data";
    size_t height = state.max_ver_fac * 8;
    size_t width = state.max_hor_fac * 8;
    Matrix y(height, width);
    Matrix cb(8, 8, 128), cr(8, 8, 128);
    Matrix aux[4];
    for (size_t i = 0; i < image.Height(); i += height) {
        for (size_t j = 0; j < image.Width(); j += width) {
            ParseMCU(reader, y, cb, cr, aux, state);
            for (size_t row = 0; row < height; ++row) {
                if (i + row >= image.Height()) {
                    break;
                }
                for (size_t col = 0; col < width; ++col) {
                    if (j + col >= image.Width()) {
                        break;
                    }
                    int cur_row = row / state.max_ver_fac;
                    int cur_col = col / state.max_hor_fac;
                    RGB pixel = YCbCrtoRGB(y.Get(row, col), cb.Get(cur_row, cur_col),
                                           cr.Get(cur_row, cur_col));
                    image.SetPixel(i + row, j + col, pixel);
                }
            }
        }
    }
    reader.ResetSkip();
    PrintTime();
}

void ParseData(BitReader& reader, Image& image, State& state) {
    unsigned char marker;
    bool found_soi = false;
    bool found_so_f0 = false;
    bool found_sos = false;
    while ((marker = reader.GetMarker()) != kEOI) {
        if (marker != kSOI && !found_soi) {
            throw std::invalid_argument{"First found marker is not SOI"};
        }
        if (found_sos) {
            throw std::invalid_argument{"Marker after end of raw data is not EOI"};
        }
        if (kAPPl <= marker && marker <= kAPPr) {
            DLOG(INFO) << "Skipping APPn section";
            ParseApplicationSpecificData(reader);
            DLOG(INFO) << "-----------------------------------------";
            continue;
        }
        switch (marker) {
            case kSOI:
                DLOG(INFO) << "Starting decoding";
                found_soi = true;
                break;
            case kCOM:
                DLOG(INFO) << "Parsing comment";
                ParseComment(reader, image);
                break;
            case kDQT:
                DLOG(INFO) << "Parsing quantization table(s)";
                ParseQuantizationTable(reader, state);
                break;
            case kSOF0:
                DLOG(INFO) << "Parsing image info";
                if (found_so_f0) {
                    throw std::invalid_argument{"Two SOF0 sections"};
                }
                ParseImageInfo(reader, image, state);
                found_so_f0 = true;
                break;
            case kDHT:
                DLOG(INFO) << "Parsing huffman tree(s)";
                ParseHuffmanTree(reader, state);
                break;
            case kSOS:
                DLOG(INFO) << "Parsing main section";
                ParseMainSection(reader, image, state);
                found_sos = true;
                break;
            default:
                DLOG(INFO) << "Unexpected marker " << int(marker);
                throw std::invalid_argument{"Unexcpected marker found"};
        }
        DLOG(INFO) << "-----------------------------------------";
    }
    DLOG(INFO) << "End of scan";
}

Image Decode(std::istream& input) {
    BitReader reader(input);
    Image image;
    State state;
    ParseData(reader, image, state);
    return image;
}
