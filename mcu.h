#pragma once

#include "bit_reader.h"
#include "common.h"
#include "huffman.h"
#include "matrix.h"

void DecodeMatrix(Matrix& matrix, Matrix& dqt);

int HuffmanTraverse(BitReader& reader, HuffmanTree& huf);

void ParseSingleMatrix(BitReader& reader, HuffmanTree& dc, HuffmanTree& ac, Matrix& matrix,
                       Matrix& dqt, State& state, int comp_id);

void ParseMCU(BitReader& reader, Matrix& y, Matrix& cb, Matrix& cr, Matrix* aux, State& state);

void PrintTime();
