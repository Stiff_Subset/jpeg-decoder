#pragma once

#include <istream>

class BitReader {
public:
    BitReader(std::istream& input) : input_(input) {
        ReadBlock();
    };

    void Shift(int offset);

    int GetMarker();

    unsigned char ReadByte() {
        return GetByte();
    }

    int ReadWord();

    int ReadBits(int count);

    void SetSkip() {
        skip_ = true;
    }

    void ReleaseLast() {
        last_ = 0;
    }

    void ResetSkip() {
        skip_ = false;
    }

private:
    unsigned char buffer_[8192];
    std::istream& input_;
    int ind_ = 0;
    int bit_ = 0;
    int size_ = 0;
    unsigned char last_ = 0;
    bool skip_ = false;

    unsigned char GetByte();

    bool GetBit();

    void ReadBlock();

    void Next();

    void NextBit();
};
