#pragma once

#include "bit_reader.h"
#include "common.h"

void ParseSingleQuantizationTable(BitReader& reader, int& length, State& state);

void ParseQuantizationTable(BitReader& reader, State& state);

void ParseSingleHuffmanTree(BitReader& reader, int& length, State& state);

void ParseHuffmanTree(BitReader& reader, State& state);
