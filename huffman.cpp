#include "huffman.h"

#include <glog/logging.h>

#include <queue>

void HuffmanTree::Build(const std::vector<uint8_t> &code_lengths,
                        const std::vector<uint8_t> &values) {
    if (code_lengths.size() > max_size_) {
        throw std::invalid_argument{"Too many rows"};
    }
    DLOG(INFO) << "Start builing huffman tree";
    CalcNodes(code_lengths);
    root_ = NewNode();
    state_ = root_;
    std::queue<std::pair<Node *, int>> q;
    q.push(std::make_pair(root_, -1));
    uint8_t cnt_inserted = 0;
    size_t length = 0;
    size_t value_ind = 0;
    DLOG(INFO) << "Start main huffman block";
    while (length != code_lengths.size()) {
        while (length < code_lengths.size() && cnt_inserted == code_lengths[length]) {
            cnt_inserted = 0;
            ++length;
        }
        if (length == code_lengths.size()) {
            return;
        }
        auto node = q.front().first;
        int level = q.front().second;
        q.pop();
        if (level < int(length)) {
            node->left = NewNode();
            node->right = NewNode();
            q.push(std::make_pair(node->left, level + 1));
            q.push(std::make_pair(node->right, level + 1));
        } else {
            if (value_ind == values.size()) {
                throw std::invalid_argument{"Not enough values"};
            }
            node->value = values[value_ind++];
            node->is_term = true;
            ++cnt_inserted;
        }
    }
    DLOG(INFO) << "Huffman tree was built";
}

bool HuffmanTree::Move(bool bit, int &value) {
    if (!state_) {
        throw std::invalid_argument{"Tree is empty"};
    }
    auto next_state = (!bit ? state_->left : state_->right);
    if (!next_state) {
        throw std::invalid_argument{"No corresponding node in tree"};
    }
    if (!next_state->is_term) {
        state_ = next_state;
        return false;
    }
    value = next_state->value;
    state_ = root_;
    return true;
}

void HuffmanTree::CalcNodes(const std::vector<uint8_t> &code_lengths) {
    DLOG(INFO) << "Calculating number of nodes";
    size_t ind = code_lengths.size();
    while (ind > 0 && code_lengths[ind - 1] == 0) {
        --ind;
    }
    size_t need = 1;
    size_t cur_number = 1;
    for (size_t i = 0; i < ind; ++i) {
        cur_number *= 2;
        need += cur_number;
        if (cur_number < code_lengths[i]) {
            throw std::invalid_argument{"Huffman tree is invalid"};
        }
        cur_number -= code_lengths[i];
    }
    DLOG(INFO) << "Number of nodes is " << need;
    node_ = 0;
    arr_.resize(need, {nullptr, nullptr, 0, false});
}
