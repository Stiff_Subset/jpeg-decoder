#pragma once

#include "bit_reader.h"
#include "matrix.h"
#include "common.h"
#include "utils/image.h"

#include <algorithm>
#include <istream>
#include <vector>

void ParseApplicationSpecificData(BitReader& reader);

void ParseComment(BitReader& reader, Image& image);

void ParseImageInfo(BitReader& reader, Image& image, State& state);

void ParseMainSection(BitReader& reader, Image& image, State& state);

void ParseData(BitReader& reader, Image& image, State& state);

Image Decode(std::istream& input);
