#include "bit_reader.h"

#include <glog/logging.h>
#include <stdexcept>

const unsigned char kZO = 0x00;
const unsigned char kFF = 0xFF;
const unsigned char kEOI = 0xD9;

void BitReader::Shift(int offset) {
    while (true) {
        int rest = size_ - ind_ - 1;
        if (rest >= offset) {
            ind_ += offset;
            return;
        }
        ReadBlock();
        offset -= rest;
    }
}

int BitReader::GetMarker() {
    while (true) {
        if (last_ == kFF) {
            if (buffer_[ind_] == kEOI) {
                return buffer_[ind_];
            }
            return GetByte();
        }
        Next();
    }
}

int BitReader::ReadWord() {
    int r = GetByte();
    int l = GetByte();
    return (r << 8) | l;
}

int BitReader::ReadBits(int count) {
    int result = 0;
    for (int i = 0; i < count; ++i) {
        result <<= 1;
        result |= GetBit();
    }
    return result;
}

unsigned char BitReader::GetByte() {
    unsigned char result = buffer_[ind_];
    Next();
    return result;
}

bool BitReader::GetBit() {
    bool result = (buffer_[ind_] & (1 << bit_)) > 0;
    NextBit();
    return result;
}

void BitReader::ReadBlock() {
    if (!input_) {
        throw std::invalid_argument{"Cannot read new block of data"};
    }
    input_.read(reinterpret_cast<char*>(buffer_), sizeof(buffer_));
    size_ = input_.gcount();
    ind_ = 0;
    bit_ = 7;
    // DLOG(INFO) << "Read " << size_ << " bytes from image";
    // DLOG(INFO) << "-----------------------------------------";
}

void BitReader::Next() {
    last_ = buffer_[ind_];
    ++ind_;
    bit_ = 7;
    if (ind_ == size_) {
        ReadBlock();
    }
    if (skip_ && last_ == kFF) {
        if (buffer_[ind_] == kZO) {
            Next();
        } else {
            throw std::invalid_argument{"Unsuppodted symbols after FF"};
        }
    }
}

void BitReader::NextBit() {
    --bit_;
    if (bit_ == -1) {
        Next();
    }
}
