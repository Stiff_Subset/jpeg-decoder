#pragma once

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <vector>

class HuffmanTree {
public:
    HuffmanTree() = default;

    void Build(const std::vector<uint8_t>& code_lengths, const std::vector<uint8_t>& values);

    bool Move(bool bit, int& value);

private:
    const size_t max_size_ = 16;

    struct Node {
        Node *left, *right;
        int value;
        bool is_term;
    };

    size_t node_;
    std::vector<Node> arr_;
    Node* root_ = nullptr;
    Node* state_ = nullptr;

    void CalcNodes(const std::vector<uint8_t>& code_lengths);

    Node* NewNode() {
        return &arr_[node_++];
    }
};
