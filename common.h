#pragma once

#include "matrix.h"
#include "huffman.h"

void Split(int value, int& dest1, int& dest2);

void Inverse(int& value, int bits);

struct State {
    Matrix quant_table[2];
    HuffmanTree huffman_tree[2][2];
    int max_hor_fac;
    int max_ver_fac;
    int components;
    int acdc_id[6];
    int dqt_id[3];
    int last_dc[3] = {};
};
