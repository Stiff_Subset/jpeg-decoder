#pragma once

#include <vector>

class Matrix {
public:
    Matrix(int height = 8, int width = 8, int default_value = 0) : height_(height), width_(width) {
        arr_.resize(height * width, default_value);
    }

    void Swap(Matrix& other) {
        arr_.swap(other.arr_);
    }

    void Set(int i, int j, int value) {
        arr_[i * width_ + j] = value;
    }

    int& Get(int i, int j) {
        return At(i * width_ + j);
    }

    int& At(int i) {
        return arr_[i];
    }

    int Height() const {
        return height_;
    }

    int Width() const {
        return width_;
    }

    int Size() {
        return arr_.size();
    }

    auto Begin() {
        return arr_.begin();
    }

    void Assign() {
        assigned_ = true;
    }

    bool IsAssigned() {
        return assigned_;
    }

    void Print();

    void Merge2x1(Matrix* aux);

    void Merge1x2(Matrix* aux);

    void Merge2x2(Matrix* aux);

private:
    std::vector<int> arr_;
    int height_, width_;
    bool assigned_ = false;
};
