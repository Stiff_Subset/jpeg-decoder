#include "common.h"

void Split(int value, int& dest1, int& dest2) {
    dest1 = value >> 4;
    dest2 = value & ((1 << 4) - 1);
}

void Inverse(int& value, int bits) {
    if ((value & (1 << (bits - 1))) > 0) {
        return;
    }
    value ^= (1 << bits) - 1;
    value = -value;
}
