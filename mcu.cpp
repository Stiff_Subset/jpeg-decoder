#include "fft.h"
#include "mcu.h"

#include <glog/logging.h>

#include <cmath>
#include <ctime>
#include <stdexcept>

double sum_inverse = 0;

const int kLayout[64] = {0,  1,  8,  16, 9,  2,  3,  10, 17, 24, 32, 25, 18, 11, 4,  5,
                         12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6,  7,  14, 21, 28,
                         35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51,
                         58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63};

void DecodeMatrix(Matrix& matrix, Matrix& dqt) {
    if (!dqt.IsAssigned()) {
        throw std::invalid_argument{"Current dqt was not assigned"};
    }
    for (int i = 0; i < 64; ++i) {
        matrix.At(i) *= dqt.At(i);
    }
    std::vector<double> input(64);
    std::vector<double> output(64);
    std::copy(matrix.Begin(), matrix.Begin() + 64, input.begin());
    DctCalculator calc(8, &input, &output);
    double t = clock();
    calc.Inverse();
    sum_inverse += clock() - t;
    for (int i = 0; i < 64; ++i) {
        matrix.At(i) = std::round(output[i]) + 128;
        matrix.At(i) = std::min(std::max(0, matrix.At(i)), 255);
    }
}

int HuffmanTraverse(BitReader& reader, HuffmanTree& huf) {
    int result;
    bool bit;
    do {
        bit = reader.ReadBits(1);
        // DLOG(INFO) << "Moving to child " << bit;
    } while (!huf.Move(bit, result));
    // DLOG(INFO) << "Current value is " << result;
    return result;
}

void ParseSingleMatrix(BitReader& reader, HuffmanTree& dc, HuffmanTree& ac, Matrix& matrix,
                       Matrix& dqt, State& state, int comp_id) {
    // DLOG(INFO) << "Finding DC value";
    int len = HuffmanTraverse(reader, dc);
    int dc_coef = 0;
    if (len != 0) {
        dc_coef = reader.ReadBits(len);
        Inverse(dc_coef, len);
    }
    dc_coef += state.last_dc[comp_id];
    state.last_dc[comp_id] = dc_coef;
    // DLOG(INFO) << "Current DC value is " << dc_coef;
    // DLOG(INFO) << "-------------------";
    matrix.At(kLayout[0]) = dc_coef;
    int ind = 1;
    while (ind < 64) {
        // DLOG(INFO) << "Finding AC value";
        int value = HuffmanTraverse(reader, ac);
        int cnt_zero, len;
        Split(value, cnt_zero, len);
        if (cnt_zero == 0 && len == 0) {
            while (ind < 64) {
                matrix.At(kLayout[ind++]) = 0;
            }
            // DLOG(INFO) << "-------------------";
            break;
        }
        for (int i = 0; i < cnt_zero; ++i) {
            matrix.At(kLayout[ind++]) = 0;
        }
        value = 0;
        if (len != 0) {
            value = reader.ReadBits(len);
            Inverse(value, len);
        }
        matrix.At(kLayout[ind++]) = value;
        // DLOG(INFO) << "Add " << cnt_zero << " zeroes and " << non_zero << " to matrix";
        // DLOG(INFO) << "-------------------";
    }
    if (ind > 64) {
        throw std::invalid_argument{"Too many values for 8x8 matrix"};
    }
    // DLOG(INFO) << "Resulting matrix:";
    // matrix.Print();
    DecodeMatrix(matrix, dqt);
    // DLOG(INFO) << "Matrix after decoding:";
    // matrix.Print();
}

void ParseMCU(BitReader& reader, Matrix& y, Matrix& cb, Matrix& cr, Matrix* aux, State& state) {
    int* acdc_id = state.acdc_id;
    int* dqt_id = state.dqt_id;
    int cnt_y = state.max_hor_fac * state.max_ver_fac;
    // DLOG(INFO) << "Y component uses " << cnt_y << " matrices";
    // DLOG(INFO) << "Parsing Y component";
    for (int i = 0; i < cnt_y; ++i) {
        ParseSingleMatrix(reader, state.huffman_tree[0][acdc_id[0]],
                          state.huffman_tree[1][acdc_id[1]], aux[i], state.quant_table[dqt_id[0]],
                          state, 0);
    }
    if (cnt_y == 1) {
        y.Swap(aux[0]);
    } else {
        if (cnt_y == 4) {
            y.Merge2x2(aux);
        } else {
            if (state.max_hor_fac == 2) {
                y.Merge1x2(aux);
            } else {
                y.Merge2x1(aux);
            }
        }
    }
    // DLOG(INFO) << "-------------------";
    if (state.components > 1) {
        // DLOG(INFO) << "Parsing Cb component";
        ParseSingleMatrix(reader, state.huffman_tree[0][acdc_id[2]],
                          state.huffman_tree[1][acdc_id[3]], cb, state.quant_table[dqt_id[1]],
                          state, 1);
        // DLOG(INFO) << "-------------------";
    }
    if (state.components > 2) {
        // DLOG(INFO) << "Parsing Cr component";
        ParseSingleMatrix(reader, state.huffman_tree[0][acdc_id[4]],
                          state.huffman_tree[1][acdc_id[5]], cr, state.quant_table[dqt_id[2]],
                          state, 2);
        // DLOG(INFO) << "-------------------";
    }
}

void PrintTime() {
    std::cerr << "Time for all inverse is " << sum_inverse / CLOCKS_PER_SEC;
}