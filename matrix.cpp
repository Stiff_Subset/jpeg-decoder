#include "matrix.h"

#include <iostream>

void Matrix::Merge1x2(Matrix* aux) {
    int size = Size();
    for (int i = 0; i < size; i += 16) {
        int shift = i >> 1;
        std::copy(aux[0].Begin() + shift, aux[0].Begin() + shift + 8, Begin() + i);
        std::copy(aux[1].Begin() + shift, aux[1].Begin() + shift + 8, Begin() + i + 8);
    }
}

void Matrix::Merge2x1(Matrix* aux) {
    std::copy(aux[0].Begin(), aux[0].Begin() + 64, Begin());
    std::copy(aux[1].Begin(), aux[1].Begin() + 64, Begin() + 64);
}

void Matrix::Merge2x2(Matrix* aux) {
    int size = Size();
    for (int i = 0; i < size / 2; i += 16) {
        int shift = i >> 1;
        std::copy(aux[0].Begin() + shift, aux[0].Begin() + shift + 8, Begin() + i);
        std::copy(aux[1].Begin() + shift, aux[1].Begin() + shift + 8, Begin() + i + 8);
    }
    int global_shift = size / 2;
    for (int i = 0; i < size / 2; i += 16) {
        int shift = i >> 1;
        std::copy(aux[2].Begin() + shift, aux[2].Begin() + shift + 8, Begin() + global_shift + i);
        std::copy(aux[3].Begin() + shift, aux[3].Begin() + shift + 8,
                  Begin() + global_shift + i + 8);
    }
}

void Matrix::Print() {
    for (int i = 0; i < height_; ++i) {
        for (int j = 0; j < width_; ++j) {
            std::cerr << Get(i, j) << "\t";
        }
        std::cerr << std::endl;
    }
}
